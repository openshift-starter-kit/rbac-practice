# RBACハンズオン

## アジェンダ
 0. [準備](#0-準備)
 1. [ハンズオン①：ClusterRoleを作成して、userX-devからProjectを閲覧する](#ハンズオン1-clusterroleを作成してuserx-devからprojectを閲覧する)
 2. [ハンズオン②：Roleを作成して、userX-devからPodを閲覧する](#ハンズオン2-roleを作成してuserx-devからpodを閲覧する)
 3. [ハンズオン③：プリセットのClusteRoleをRolebindingしてPodを作成する](#ハンズオン3-プリセットのclusteroleをrolebindingしてpodを作成する)
 4. [ハンズオン④：Groupに対してClusterRoleをClusterRoleBindingする](#ハンズオン4-groupに対してclusterroleをclusterrolebindingする)

## 0. 準備

### ユーザアカウントの割り当て
---
* Etherpad にて、user1 / user1-dev ... の箇所へ、メールアドレスを入力してください。ユーザの重複はできないので、
空いている箇所へ記入をお願いします。  
* このハンズオンでは一人につき二つのアカウント(userX/userX-dev)を払い出しています。
手順に従って、適宜ログイン・ログアウトを実施しながら進めてください。  
* 手順内に"userX"や"userX-dev"といった記載がありますが、Xの部分を自分のアカウントの番号に置き換えて実施ください。

## ハンズオン1. ClusterRoleを作成して、userX-devからProjectを閲覧する

ここではCluster-adminの権限を持つuserXが作成したProjectを、ClusterRoleを使ってuserX-devから閲覧できるようにします。  


### このハンズオンの目的
---
* ClusterRoleとRoleBindingを使うことで、特定のProject内におけるOpenShiftの操作権限をユーザーに付与できることを確認する。
* 付与した権限以外の操作ができないことを確認する。


### 1-1. userXでログイン
---
WebブラウザよりOpenShift Web Conosole へアクセスします。  
※URLはEtherpadの環境情報"Openshift Console"を参照  
"userX"でログインします。  
![handson-0](./images/login_userx.png)  


### 1-2. Projectの作成
---
左側メニューから"Home"→"Projects"を選択します。  
![handson](./images/menu_projects.png)

画面右側の"Create Project"を選択します。  
![handson](./images/create_project.png)

以下の内容を入力し、"Create"を選択します。
* Name : userX-rbac
* その他 : デフォルトのまま

![handson](./images/create_userx-rbac_project.png)

Project:"userX-rbac"が作成されていることを確認します。  

![handson](./images/userx-rbac_project_view.png)


### 1-3. Podの作成
---
次に作成したProjectにPodを作成しておきます。  
左側メニューから"Workloads"→"Pods"を選択します。  
![handson](./images/menu_pods.png)

左上のProjectが"userX-rbac"になっていることを確認して、画面右上の"Create Pod"を選択します。  
![handson](./images/create_pod.png)

特に変更を加えることなく、画面下の"Create"を選択します。  
![handson](./images/create_example_pod.png)

Pod:"example"が作成されていることを確認します。  
![handson](./images/example_pod_view.png)


### 1-4. ClusterRoleの作成
---
user-devXからProjectを閲覧できるようにするためのClusterRoleを作成します。  
左側メニューから"User Management"→"Roles"を選択します。  
![handson](./images/menu_roles.png)

画面右上の"Create Role"を選択します。  
![handson](./images/create_role.png)

yamlの編集画面が表示されるので、以下の内容に書き換えます。  
**※"name: userX-view-project"のXは自分のアカウントの通番に置き換えてください。**

中身をみると、resourcesで"projects"と"namespaces"の閲覧権限を付与していることが分かります。  
OpenShiftではKubernetesのNamespaceをProjectというオブジェクトでオーバーラップしているため、Projectの閲覧のためにNamespaceの閲覧権限も付与します。  
```
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: userX-view-project
rules:
  - apiGroups:
      - ''
    resources:
      - namespaces
    verbs:
      - get
      - list
      - watch
  - apiGroups:
      - 'project.openshift.io'
    resources:
      - projects
    verbs:
      - get
      - watch
      - list
```

![handson](./images/create_clusterrole_userx-view-project.png)


"Create"を選択すると、ClusterRole:"userX-view-project"が作成されます。  
![handson](./images/userx-view-project_view.png)


### 1-5. RoleBindingの作成
---
ProjectとClusterRoleが作成できたので、最後にRoleBindingを作成してuserX-devに権限を付与します。  
左側メニューから"User Management"→"RoleBindings"を選択します。  
![handson](./images/menu_rolebindings.png)

左上のProjectが"userX-rbac"になっていることを確認して、画面右上の"Create binding"を選択します。  
![handson](./images/create_binding.png)

以下の内容を入力して、"Create"を選択します。  
* Binding type : Namespace role binding(RoleBinding)
* RoleBinding
  * Name : view-project-rolebinding
  * Namespace : userX-rbac
* Role
  * Role name : userX-view-project
* Subject : User
  * Subject name : userX-dev

![handson](./images/create_rolebinding_view-project1.png)  
![handson](./images/create_rolebinding_view-project2.png)  

画面でRoleBindingが作成されていることを確認します。  
![handson](./images/view-project-rolebinding_view.png)  

この作業によって、Project:**userX-rbac**において、**userX-view-project**というRoleが**view-project-rolebinding**で**userX-dev**に対して付与されました。  
これでuserX-devはuserX-rbac Projectの閲覧権限を持ったことになります。  
実際に権限を確認してみましょう。


### 1-6. userX-devにアカウント切り替え
---
権限の確認のため、user-X-devでログインします。  
まずuserXのログアウトを実施します。  
右上から"userX"→"ログアウト"を選択します。  
![handson](./images/logout_userx.png)  

"userX-dev"でログインし直します。
![handson](./images/login_userx-dev.png)  


### 1-7. Projectの表示を確認する
---
ログインすると"Topology"でuserXが作成したProject:"user1-rbac"が表示されています。  
本来ならまだuserX-devでProjectは作成していないのでここは空欄になりますが、先ほどProject:"userX-rbac"への閲覧権限を付与したため、ここで確認できるようになりました。  
![handson](./images/topology_view.png)  

ではこの"Topology"画面でProject:"userX-rbac"を選択してみて下さい。  
すると以下のような画面が表示されるかと思います。  
![handson](./images/topology_view_project.png)  

"No resource found"とのことですが、先ほどこのProjectの中にPodを作成したので本来ならここにPodが表示されるはずです。  
しかし、userX-devにはProjectの閲覧権限しか付与していないため、Podの確認ができません。
次のハンズオンで、このPodが確認できるように、userX-devに権限を追加していきましょう。


## ハンズオン2. Roleを作成して、userX-devからPodを閲覧する

ハンズオン1では、ClusterRoleとRoleBindingを使って権限付与を行いました。  
このハンズオンでは、Project内に作成するRoleを使ってuserX-devにPodの閲覧権限を付与していきます。


### このハンズオンの目的
---
* RoleとRoleBindingを使うことで、特定のProject内におけるOpenShiftの操作権限をユーザーに付与できることを確認する。
* 付与した権限以外の操作ができないことを確認する。


### 2-1. userXにアカウント切り替え
---

userX-devのログアウトを実施します。  
右上から"userX-dev"→"ログアウト"を選択します。  
![handson](./images/logout_userx-dev.png)  

"userX"でログインし直します。  
![handson](./images/login_userx.png)  


### 2-2. Roleの作成
---
user-devXからPodを閲覧できるようにするためのRoleを作成します。  
左側メニューから"User Management"→"Roles"を選択します。  
![handson](./images/menu_roles.png)

左上のProjectが"userX-rbac"になっていることを確認して、画面右上の"Create Role"を選択します。  
![handson](./images/create_role.png)

yamlの編集画面が表示されます。  
実はデフォルトで表示されるyamlが「Podの閲覧権限を付与する」記載になっているので、rules配下の変更は不要です。  
nameだけ以下のように書き換えて"Create"を選択しましょう。  
* name : view-pods-role

![handson](./images/create_view-pods-role.png)


Role:"view-pods-role"ができていることを確認します。  
![handson](./images/view-pods-role_view.png)


### 2-3. RoleBindingの作成
---
Roleが作成できたので、RoleBindingを作成してuserX-devに権限を付与します。  
左側メニューから"User Management"→"RoleBindings"を選択します。  
![handson](./images/menu_rolebindings.png)

左上のProjectが"userX-rbac"になっていることを確認して、画面右上の"Create binding"を選択します。  
![handson](./images/create_binding.png)

以下の内容を入力して、"Create"を選択します。  
* Binding type : Namespace role binding(RoleBinding)
* RoleBinding
  * Name : view-pods-rolebinding
  * Namespace : userX-rbac
* Role
  * Role name : view-pods-role
* Subject : User
  * Subject name : userX-dev

![handson](./images/create_rolebinding_view-pods1.png)  
![handson](./images/create_rolebinding_view-pods2.png)  

画面でRoleBindingが作成されていることを確認します。  
![handson](./images/rolebinding_view-pods_view.png)  


### 2-4. userX-devにアカウント切り替え
---
権限の確認のため、user-X-devでログインします。  
まずuserXのログアウトを実施します。  
右上から"userX"→"ログアウト"を選択します。  
![handson](./images/logout_userx.png)  

"userX-dev"でログインし直します。
![handson](./images/login_userx-dev.png)  


### 2-5. Projectの表示を確認する
---
ログインして"Topology"でProject:"userX-rbac"を選択すると、先ほどは表示されていなかったところにuserXが作成したPod:"example"が表示されています。  
![handson](./images/topology_view2.png)  

これでハンズオン1と合わせて以下の状態になりました。
* **userX-dev**は**userX-rbac**Projectにおいて、ProjectとPodsの閲覧ができる
  * ClusterRoleとRolebindingを使ってProjectの閲覧権限を付与
  * RoleとRoleBindingを使ってPodの閲覧権限を付与



しかし、このように権限不足を見つけては必要な権限を調べて付与して..を繰り返していても、自分たちの実現したい権限を持った状態にするのはとても時間がかかってしまいます。  
そこでOpenShiftではいくつかの利用目的を考慮したプリセットの権限（ClusterRole）を準備しています。  
次のハンズオンでは、このプリセットのClusterRoleを使った権限付与を行います。


## ハンズオン3. プリセットのClusteRoleをRolebindingしてPodを作成する

ここでは、OpenShiftのプリセットにあるClusterRoleを使って、userX-devに対しPodの作成権限を付与します。  


### このハンズオンの目的
---
* プリセットのClusterRoleを活用することで、効率よく目的に沿った操作権限をユーザーに付与できることを確認する。


### プリセットClusterRoleのおさらい
---
前段の講義でも触れましたが、OpenShiftではプリセットとしていくつかのClusterRoleを準備しています。その中でもよく使うことの多いRoleを以下にまとめます。
* cluster-admin : クラスター内の全てのアクションを実行できる
* admin : Project内のQuota以外のリソースを閲覧・変更できる
* edit : Project内のQuotaやRole/Rolebinding以外のリソースを閲覧・変更できる
* view : Project内のほとんどのオブジェクトを閲覧できる
* basic-user : ProjectやUserなど、クラスターレベルの基本的な情報を閲覧できる

cluster-adminは全権限を持ち、逆にbasic-userはかなり限定的な権限しか持っていないため、特定のProjectにRoleBindingする際は"admin","edit","view"の3つから選択するケースが多いです。  
今回はPodの作成権限を付与するため、"edit"をuserX-devに付与してみましょう。


### 3-1. userXにアカウント切り替え
---
userX-devのログアウトを実施します。  
右上から"userX-dev"→"ログアウト"を選択します。  
![handson](./images/logout_userx-dev.png)  

"userX"でログインし直します。  
![handson](./images/login_userx.png) 


### 3-2. RoleBindingの作成
---
もう慣れてきましたね。RoleBindingを作成してuserX-devに権限を付与していきます。  
左側メニューから"User Management"→"RoleBindings"を選択します。  
![handson](./images/menu_rolebindings.png)

左上のProjectが"userX-rbac"になっていることを確認して、画面右上の"Create binding"を選択します。  
![handson](./images/create_binding.png)

以下の内容を入力して、"Create"を選択します。  
* Binding type : Namespace role binding(RoleBinding)
* RoleBinding
  * Name : edit-rolebinding
  * Namespace : userX-rbac
* Role
  * Role name : edit
* Subject : User
  * Subject name : userX-dev

![handson](./images/create_edit-rolebinding1.png)  
![handson](./images/create_edit-rolebinding1.png)  

画面でRoleBindingが作成されていることを確認します。  
![handson](./images/edit-rolebinding_view.png)  


### 3-3. userX-devにアカウント切り替え
---
権限の確認のため、user-X-devでログインします。  
まずuserXのログアウトを実施します。  
右上から"userX"→"ログアウト"を選択します。  
![handson](./images/logout_userx.png)  

"userX-dev"でログインし直します。
![handson](./images/login_userx-dev.png)  


### 3-4. Projectの表示を確認する
---
Podを作成できるか確認してみましょう。  
まず管理用画面に切り替えます。  
メニュー左上の"Developer"プルダウンを選択して"Administorator"を選択します。  
![handson](./images/console_administrator.png)  

左側メニューから"Projects"を選択します。  
![handson](./images/menu_projects.png)  

Project:"userX-rbac"が選択されていることを確認し、"Create Pod"を選択します。  
![handson](./images/create_pod.png)  

Podのnameが既存のPodとかぶるとエラーが出てしまうので、ここでnameを"example2"に書き直して"Create"を選択します。  
![handson](./images/create_example2_pod.png)  

するとexample2のPodが作成されたかと思います。  
![handson](./images/create_example2_pod_view.png)  

ハンズオン2までではPodに対しては閲覧のみの権限付与でしたが、"edit"のClusterRoleを付与することでPodの作成ができるようになりました。  
またPodだけでなく、Day1で実施したサンプルアプリのデプロイもこの"edit"権限を付与することでできるようになりましたので、時間のある方は試してみてください。

## ハンズオン4. Groupに対してClusterRoleをClusterRoleBindingする

ここまでのハンズオンでは、RoleBindingを使ってuseX-devというUserにProject:"userX-rbac"内における権限を付与してきました。  
しかしクラスター運用の中では、クラスターレベルのオブジェクトにアクセスしたり、複数のProject内のオブジェクトを閲覧・変更する作業も出てきます。  
またそういったクラスター全体の作業は一人だけで行うことはほとんどなく、複数のユーザーが同じレベルの権限を持って管理することが多いと思います。  
そのためここではハンズオンの最後として、UserではなくGroupに対してクラスター全体を操作する権限を付与すべく、ClusterRoleBindingを作成します。  


### このハンズオンの目的
---
* ClusterRoleとClusterRoleBindingを使うことで、クラスター全体におけるOpenShiftの操作権限をユーザーに付与できることを確認する。
* グループを使うことで複数ユーザーに効率的に権限付与を実施することができる


### 4-1. userXにアカウント切り替え
---
userX-devのログアウトを実施します。  
右上から"userX-dev"→"ログアウト"を選択します。  
![handson](./images/logout_userx-dev.png)  

"userX"でログインし直します。  
![handson](./images/login_userx.png)  


### 4-2. groupの作成
---
左側メニューから"User Management"→"Groups"を選択します。  
![handson](./images/menu_groups.png)  

右側の"Create Group"を選択します。  
![handson](./images/create_group.png)  

以下の内容を入力して"Create"を選択します。
* name : userX-group
* users
   * userX
   * userX-dev  

![handson](./images/create_userx-group.png)  

Groupが作成されたことを確認します。  
![handson](./images/userx-group_view.png)


### 4-3. ClusterroleBindingの作成
---
今回はcluster-adminというRoleをClusterRoleBindingでuserX-groupに付与します。  
左側メニューから"User Management"→"RoleBindings"を選択します。  
![handson](./images/menu_rolebindings.png)  

左上のProjectが"userX-rbac"になっていることを確認して、画面右上の"Create binding"を選択します。  
![handson](./images/create_binding.png)  

以下の内容を入力して、"Create"を選択します。  
* Binding type : Cluster-wide role binding (ClusterRoleBinding)
* RoleBinding
  * Name : userX-cluster-admin
  * Namespace : userX-rbac
* Role
  * Role name : cluster-admin
* Subject : Group
  * Subject name : userX-group

![handson](./images/create_userx-cluster-admin_1.png)  
![handson](./images/create_userx-cluster-admin_2.png)  

画面でRoleBindingが作成されていることを確認します。  
![handson](./images/userx-cluster-admin_view.png)  


### 4-4. userX-devにアカウント切り替え
---
権限の確認のため、user-X-devでログインします。  
まずuserXのログアウトを実施します。  
右上から"userX"→"ログアウト"を選択します。  
![handson](./images/logout_userx.png)  

"userX-dev"でログインし直します。
![handson](./images/login_userx-dev.png)  

---
### 4-5. 権限の確認
---
メニュー左上の"Developer"プルダウンを選択して"Administorator"を選択します。  
![handson](./images/console_administrator.png) 

するとこれまで見えていなかった"Overview"が見えたり、他の参加者が作成したProjectを閲覧できるようになりました。  
![handson](./images/overview.png) 


これはgroupにcluster-adminというクラスター内におけるスーパーユーザーの権限を付与することで、そのgroupに所属しているユーザー（ここではuserX-dev）がcluster-admin権限を持ったことになります。  
このように、"インフラ管理者"や"開発者"など、役職に応じて均一な権限を付与する場合はgroupに対してClusterRoleをBindingする運用にすることで権限管理の稼働を効率化できます。


## おわりに

以上でRBACのハンズオンは終了です。  
コンテナ環境ではこれまでのVM環境のように、HyperVisorは運用者、OS以上は開発者という風に利用するツールを完全に分断することができません。  
開発者も運用者もコンテナプラットフォームをうまく活用してDevOpsを実現することから、RBACでの権限管理は必ず必要になる概念です。  
今回の手順やデプロイしたオブジェクトの概要をきちんと理解し、適切な権限管理を実現いただければと思います。  
お疲れ様でした。


